'use strict';
/**
 * @ngdoc function
 * @name VettiverFaxApp.controller:PublicCtrl
 * @description
 * # PublicCtrl
 * Controller of the VettiverFaxApp
 */
angular.module('VettiverFaxApp')
  .controller('PublicCtrl', function ($scope, Campaign, $location, $sce) {
    var campaignId = $location.search()['campaignId'] || '/';
    if (campaignId === '/') {
      window.location.href = '/login.html#/?';
    }

    toastr.options.closeButton = true;

    $scope.signature = {
      country: 'IN'
    };

    $scope.campaign = Campaign.findOne({
      filter: {
        where: {
          name: campaignId
        }
      }
    }, function onSuccess() {
    }, function onError() {
      window.location.href = '/login.html#/?';
    });
    $scope.$watch(function () {
      var campaign = $scope.campaign;
      if (campaign.description && campaign.description.indexOf('\n') > -1) {
        campaign.description =
          campaign.description.replace('\n','<br/>');
      }
      if (campaign.faxBody && campaign.faxBody.indexOf('\n') > -1) {
        campaign.faxBody =
          campaign.faxBody.replace('\n','<br/>');
      }
    });

    $scope.save = function (signature) {
      Metronic.blockUI({
        target: '#signature',
        animate: true
      });
      Campaign.prototype$__create__signatures({id: $scope.campaign.id},
        signature,
        function onSuccess() {
          Metronic.unblockUI('#signature');
          toastr.success('Thank you for signing the petition! Your fax will be sent out as soon as possible.');
        }, function onError() {
          toastr.error('There was an error registering your signature! Please make sure you haven\'t used your email already.');
        });
    };
  });
