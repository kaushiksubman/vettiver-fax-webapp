'use strict';
/**
 * @ngdoc function
 * @name VettiverFaxApp.controller:CampaignsCtrl
 * @description
 * # CampaignsCtrl
 * Controller of the VettiverFaxApp
 */
angular.module('VettiverFaxApp')
  .controller('CampaignsCtrl',
  function ($scope, $state, $stateParams, Campaign, $log) {
    $scope.where = decodeStateParams($stateParams);
    $scope.subPage = null;
    if ($scope.where.status === 'pending') {
      $scope.subPage = 'Pending Campaigns';
    }
    $scope.page = parseInt($stateParams.page || 1);
    $scope.pageSize = 10;
    $scope.totalItems = $scope.pageSize * $scope.page;

    var filter = paramsToFilter($scope.where);
    if (filter.where && filter.where.title && filter.where.title.like) {
      var like = filter.where.title.like;
      filter.where.title = {
        like: like + '.*',
        options: 'i'
      };
    }

    $log.debug('Campaign.count:',
      JSON.stringify({where: filter.where}));
    var countResult = Campaign.count({where: filter.where},
      function onSuccess() {
        if ($scope.totalItems > (countResult.count + $scope.pageSize)) {
          $scope.totalItems = countResult.count;
        }
        else {
          $scope.totalItems = countResult.count;
          $scope.campaigns =
            Campaign.find({filter: filter}, function onSuccess() {
              $log.debug('Campaign.find Success');
              for (var i = 0; i < $scope.campaigns.length; i++) {
                $scope.campaigns[i].faxNumbers = $scope.campaigns[i].faxNumbers.join(", ");
              }
            }, function onError() {
              $log.error('Campaign.find Error');
            });
        }
      }, function onError() {
        $log.error('Error');
      });

    $scope.search = function () {
      var stateParams = encodeStateParams($scope.where);
     // console.log($state.go('.', stateParams));
      $state.go('.', stateParams);
    };
    $scope.reset = function () {
      for (var key in $scope.where) {
        $scope.where[key] = undefined;
      }
      $scope.search();
    };
    if (jQuery().datepicker) {
      $('.date-picker').datepicker({
        rtl: Metronic.isRTL(),
        autoclose: true
      });
    }
    else {
      $log.error('datepicker not present');
    }

    $scope.open = function ($event, selector) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.opened = $scope.opened || {};
      $scope.opened[selector] = true;
    };

    function encodeStateParams(params) {
      var stateParams = angular.copy(params);
      for (var key in stateParams) {
        var prop = stateParams[key];
        //no need to encode if it's a string
        if (typeof prop === 'string') {
          continue;
        }
        //if it's an object, remove empty properties
        if (typeof prop === 'object') {
          //loop and remove empty properties
          for (var k in prop) {
            var p = prop[k];
            if (p === undefined) {
              delete prop[k];
            }
          }
        }
        //set keys with empty objects as undefined
        if ($.isEmptyObject(prop)) {
          stateParams[key] = undefined;
          continue;
        }
        else {
          $log.debug('else:', key, prop);
        }
        var str = JSON.stringify(prop);
        stateParams[key] = str;
      }
      if (stateParams.trending === 'false') {
        stateParams.trending = false;
      }
      if (stateParams.trending === 'true') {
        stateParams.trending = true;
      }
      stateParams.page = $scope.page;
      return stateParams
    }

    function decodeStateParams($stateParams) {
      var params = angular.copy($stateParams) || {};
      for (var key in params) {
        var prop = params[key];
        if (typeof prop !== "string") {
          continue;
        }
        try {
          var obj = JSON.parse(decodeURIComponent(prop));
          params[key] = obj;
        }
        catch (e) {
          //Do nothing
        }
      }
      if (params.trending === false) {
        params.trending = 'false';
      }
      if (params.trending === true) {
        params.trending = 'true';
      }
      delete params.page;
      return params;
    }

    function paramsToFilter(params) {
      var where = angular.copy(params);
      var skip = $scope.pageSize * ($scope.page - 1);
      return {
        where: where,
        skip: skip,
        limit: $scope.pageSize
      };
    }

    $scope.getDisplayName = function (user) {
      if (!user) {
        return 'undefined';
      }
      if (user.fullName) {
        return user.fullName;
      }
      var dispName = '';
      if (user.firstName) {
        dispName += user.firstName;
      }
      if (user.lastName) {
        if (user.firstName) {
          dispName += ' ';
        }
        dispName += user.lastName;
      }
      if (dispName !== '') {
        return dispName;
      }
      dispName = user.username;
      return dispName;
    };
    $scope.pageChanged = function (value) {
      console.log('page pageChanged',value);
      $scope.search();
    };
    toastr.options.closeButton = true;
    $scope.approve = function (campaign) {
      Campaign.prototype$patchAttributes({
        id: campaign.id
      }, {
        status: 'active'
      }, function onSuccess(updatedCampaign) {
        campaign.status = updatedCampaign.status;
        toastr.success('Campaign has been Approved!',
          'Campaign Approved');
      }, function onError() {
        $log.error('error approving campaign');
      })
    };
    $scope.rejected = function (campaign) {
      console.log(campaign);
      Campaign.prototype$patchAttributes({
        id: campaign.id
      }, {
        status: 'rejected'
      }, function onSuccess(updatedCampaign) {
        campaign.status = updatedCampaign.status;
        console.log(campaign.status);
      }, function onError() {
        $log.error('error in rejecting campaign');
      })
    };
  });
