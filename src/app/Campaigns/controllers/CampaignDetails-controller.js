'use strict';
/**
 * @ngdoc function
 * @name VettiverFaxApp.controller:CampaignDetailsCtrl
 * @description
 * # CampaignDetailsCtrl
 * Controller of the VettiverFaxApp
 */
angular.module('VettiverFaxApp').controller('CampaignDetailsCtrl',
  ['$scope', 'campaign','$timeout','Campaign', 'dialogs', '$state', '$stateParams', 'User', '$log', '$http', 'EnvironmentConfig', 'LoopBackAuth',
    function ($scope, campaign, $timeout, Campaign, dialogs, $state, $stateParams, User, $log, $http, EnvironmentConfig, LoopBackAuth) {
      if (!campaign) {
        campaign = new Campaign();
      }
      $scope.domainAdmin = User.getCachedCurrent();
      $scope.subPage = 'Create';
      $scope.isCreate = true;
      $scope.newPost = {};
      $scope.editMode = false;
      if (campaign.id) {
        if (campaign._faxFile) {
          var faxFile = campaign._faxFile;
          var fileNameArray = faxFile.title.split('/');
          $scope.fileName = fileNameArray[fileNameArray.length - 1];
        }
        $scope.subPage = 'Edit';
        $scope.isCreate = false;
      }
      else {
        $scope.editMode = true;
      }
      $scope.signaturePageSize = 10;

      $scope.deletePage = 1;

      $scope.$watch(function () {
        if (campaign && !$scope.editMode) {
          if (campaign.description && campaign.description.indexOf('\n') > -1) {
            campaign.description =
              campaign.description.replace('\n','<br/>');
          }
          if (campaign.description && campaign.faxBody &&
            campaign.faxBody.indexOf('\n') > -1) {
            campaign.faxBody =
              campaign.faxBody.replace('\n','<br/>');
          }
        }
        if (campaign.description && campaign && $scope.editMode) {
          if(campaign.description.indexOf('<br/>') > -1) {
            campaign.description =
              campaign.description.replace('<br/>','\n');
          }
          if (campaign.description && campaign.faxBody &&
            campaign.faxBody.indexOf('<br/>') > -1) {
            campaign.faxBody =
              campaign.faxBody.replace('<br/>','\n');
          }
        }
      });
      $scope.alert = {
        type: 'success',
        msg: 'hello there'
      };
      if (!$scope.isCreate) {
        var signatureFilter = paramsToFilter(1);
        signatureFilter.order = 'created DESC';
        Campaign.prototype$__get__signatures({
            id: campaign.id,
            filter: signatureFilter
          },
          function onSuccess(signatures) {
            $scope.campaignSignatures = signatures;
            //console.log('total members',memberships.length)
          });
        Campaign.prototype$__count__signatures({
            id: campaign.id,
            filter: {
              order: 'created DESC'
            }
          },
          function onSuccess(signatures) {
            $scope.signatureCount = signatures.count;
            //console.log('total members',memberships.length)
          });
      }
      $scope.pristineCampaign = angular.copy(campaign);
      $scope.campaign = campaign;

      $scope.clear = function () {
        $scope.dt = null;
      };

      $scope.open = function ($event, selector) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = $scope.opened || {};
        $scope.opened[selector] = true;
      };

      $scope.openTab = function (url) {
        var win = window.open(url, '_blank');
        win.focus();
      };
      $scope.clickToOpen = function () {
        $scope.campaign.type = "campaign";
        var dlg = dialogs.create('app/tpl/edit-image-dialog.html',
          'EditImageCtrl',
          $scope.campaign);
        dlg.result.then(function (value) {
          $scope.onImageUploadSuccess(value);

          //TODO: We should probably save the data right away.As of now, data is not saved. User has to click
          //the save button to activate the new profile picture
        }, function (value) {
          //TODO: handle error
        });
      };
      $scope.save = function (campaign) {
        console.log("in save");
        Metronic.blockUI({
          target: '#campaign_details_form_portlet',
          animate: true
        });
        var postData = {};
        for (var property in campaign) {
          if (!campaign.hasOwnProperty(property)) {
            continue;
          }
          if (['title', 'name', 'description', 'faxTo', 'faxMessage',
              'faxBody', 'faxNumbers', 'target', 'status'].indexOf(property) > -1) {
            if (property === 'faxNumbers' && typeof campaign[property] === 'string') {
              console.log(campaign[property]);
              campaign[property] = campaign[property].split(',');
            }
            postData[property] = campaign[property];
          }
          //set `_picture` property in case of createCampaign
          if (property === '_picture' && !campaign.id) {
            postData[property] = campaign[property];
          }
          //set `_faxFile` property in case of createCampaign
          if (property === '_faxFile' && !campaign.id) {
            postData[property] = campaign[property];
          }
        }
        toastr.options.closeButton = true;
        if (campaign.id) {
          Campaign.prototype$patchAttributes({
            id: campaign.id
          }, postData, function onSuccess(updatedCampaign) {
            toastr.success('Campaign has been saved successfully!',
              'Campaign Saved');

            $scope.campaign = updatedCampaign;
            $scope.pristineCampaign = angular.copy($scope.campaign);
            Metronic.unblockUI('#campaign_details_form_portlet');
            $scope.editMode = false;
            $state.go('campaigns.details', {id: $scope.campaign.id});
          }, function onError() {
            $log.error('error');
          });
        }
        else {
          console.log("in create Campaign",postData);
          Campaign.create(postData, function onSuccess(newCampaign) {
            console.log("success");
            toastr.success('Campaign has been created successfully!',
              'Campaign Created');
            $scope.campaign = newCampaign;
            Metronic.unblockUI('#campaign_details_form_portlet');
            $state.go('campaigns.details', {id: newCampaign.id})
          }, function onError() {
            $log.error('error');
          });
        }
      }
      $scope.isChanged = function () {
        var m = $scope.campaign;
        var p = $scope.pristineCampaign;
        var fields = ['title', 'name', 'description', 'faxTo', 'faxMessage',
          'faxBody', 'faxNumbers','target', 'status', '_picture', '_faxFile'];
        if ($scope.editMode) {
          for (var i = 0; i < fields.length; i++) {
            var field = fields[i];
            if (typeof m[field] === 'object' && typeof p[field] === 'object') {
              if (m[field].id && m[field].id !== p[field].id) {
                return false;
              }
            }
            else if (m[field] !== p[field]) {
              return false;
            }
          }
          return true;
        }
      }

      $scope.submitDisabled = function () {
        var m = $scope.campaign;
        var p = $scope.pristineCampaign;
        var requiredFields = ['title', 'description', 'name',
          'faxTo', 'faxBody', 'faxNumbers', 'target']; //created and signatureCount have default values
        var changed = false;
        var allFieldsSet = true;
        for (var i = 0; i < requiredFields.length; i++) {
          var field = requiredFields[i];
          if (!m[field]) {
            allFieldsSet = false;
            break;
          }
          if (m[field] !== p[field]) {
            changed = true;
          }
        }
        var otherFields = ['faxMessage', 'status', '_picture', '_faxFile'];
        for (var i = 0; i < otherFields.length; i++) {
          var field = otherFields[i];
          if (typeof m[field] === 'object' && typeof p[field] === 'object') {
            if (m[field].id && m[field].id !== p[field].id) {
              changed = true;
              break;
            }
          }
          else if (m[field] !== p[field]) {
            changed = true;
            break;
          }
        }
        return !(changed && allFieldsSet);
      };

      $scope.cancelEdit = function () {
        if ($scope.isCreate) {
          return $state.go('campaigns.list');
        }
        $scope.editMode = false;
        $state.go('campaigns.details', {id: campaign.id});
      };

      $scope.onImageUploadSuccess = function (data) {
        if ($scope.campaign.id) {
          Campaign.prototype$linkPicture({
            id: $scope.campaign.id
          }, data, function onSuccess(value) {
            $log.debug('image linked to campaign:', $scope.campaign.id);
            $scope.campaign._picture = value;
          }, function onError() {
            //TODO: handle error
            $log.error('error');
          });
        }
        else {
          $scope.campaign._picture = data;
        }
      };

      $scope.pageChanged = function (value) {
        $scope.deletePage = value;
        var signatureFilter = paramsToFilter(value);
        Campaign.prototype$__get__signatures({
            id: campaign.id,
            filter: signatureFilter
          },
          function onSuccess(signatures) {
            $scope.campaignSignatures = signatures;
          });
      };

    /*function decodeStateParams($stateParams) {
      var params = angular.copy($stateParams) || {};
      delete params.page;
      return params;
    }*/

    function paramsToFilter(value) {
      var skip = $scope.signaturePageSize * (value - 1);
      return {
        offset: skip,
        limit: $scope.signaturePageSize
      };
    }

    $scope.fileUpload = function (file, invalidFiles) {
      if (invalidFiles.length) {
        alert('Invalid File Type');
        return;
      }
      Metronic.blockUI({
        target: '#campaign_details_form_portlet',
        animate: true
      });
      var formData = new FormData();
      formData.append('file', file);
      $http.post(EnvironmentConfig.API_URL + '/users/me/files/upload',
        formData, {
          headers: {
            'Content-Type': undefined,
            authorization: LoopBackAuth.accessTokenId
          }
        }).success(onFileUploadSuccess).error(function onError() {
          Metronic.unblockUI('#campaign_details_form_portlet');
          $log.error('error');
          //TODO: handle error
        });
      function onFileUploadSuccess(data, status, headers, config) {
        Metronic.unblockUI('#campaign_details_form_portlet');
        if ($scope.campaign.id) {
          Campaign.prototype$linkFile({
            id: $scope.campaign.id
          }, data, function onSuccess(value) {
            $log.debug('file linked to campaign:', $scope.campaign.id);
            $scope.campaign._faxFile = value;
            var faxFile = campaign._faxFile;
            var fileNameArray = faxFile.title.split('/');
            $scope.fileName = fileNameArray[fileNameArray.length - 1];
          }, function onError() {
            //TODO: handle error
            $log.error('error');
          });
        } else {
          $scope.campaign._faxFile = value;
        }
        $log.debug('file upload success');
      }
    };
    $scope.allSignaturesDow =
      Campaign.prototype$__get__signatures({
        id: campaign.id,
        filter: {}
      }, function onSuccess() {
          $log.debug('Signature.find Success');
          var signatureLength = $scope.allSignaturesDow.length;
          $scope.dataDow=[];
          for(var i = 0; i < signatureLength; i++) {
            $scope.dataDow.push({name:$scope.allSignaturesDow[i].firstName + $scope.allSignaturesDow[i].lastName,
              email: $scope.allSignaturesDow[i].email,
              phone: $scope.allSignaturesDow[i].phone,
              postalCode: $scope.allSignaturesDow[i].postalCode,
              country: $scope.allSignaturesDow[i].country,
              Time: (new Date($scope.allSignaturesDow[i].created)).toDateString()
            });
          }
      }, function onError() {
        $log.error('User.find Error');
      });
}]);
