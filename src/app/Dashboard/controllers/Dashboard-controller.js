'use strict';
/**
 * @ngdoc function
 * @name VettiverFaxApp.controller:dashboardCtrl
 * @description
 * # dashboardCtrl
 * Controller of the VettiverFaxApp
 */
angular.module('VettiverFaxApp')
  .controller('DashboardCtrl',
  ['$rootScope', '$scope', '$http', '$timeout', '$injector', '$state', '$log',
    'User',
    function ($rootScope, $scope, $http, $timeout, $injector, $state, $log,
              User) {
      $scope.$on('$viewContentLoaded', function () {
        // initialize core components
        Metronic.initAjax();
      });
      // success response

      $scope.domainAdmin = User.getCachedCurrent();
      var Campaign = $injector.get('Campaign', 'DashboardCtrl');
      var Signature = $injector.get('Signature', 'DashboardCtrl');      
      $scope.campaigns = Campaign.count({}, function () {
        $log.debug($scope.campaigns);
      }, function () {
        $log.error('error');
      });

      $scope.totalSignatures = Signature.count({}, function () {
        $log.debug($scope.totalSignatures);
      }, function () {
        $log.debug($scope.totalSignatures);        
      })

      User.count(function onSuccess(res){
        $scope.totalUsers=res.count;
      });

      $scope.allSignatures = function () {
        $scope.signatures =
          Signature.find({filter: {
            order: 'created DESC',
            include: 'campaign',
            limit: 50
          }
        }, function success(signatures) {
          console.log(signatures);
        }, function error(errors) {

        });
      };
      $scope.allSignatures();
      $scope.handleSignature = function (signature) {
        var stateParams = {
          id: signature.campaignId
        };
        $state.go('campaigns.details', stateParams);
      };
    }]);
