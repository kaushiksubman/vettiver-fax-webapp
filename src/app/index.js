'use strict';

(/**
 *
 * @param {Metronic} Metronic
 */
  function (Metronic) {
  var VettiverFaxApp = angular.module('VettiverFaxApp',
    ['VettiverFaxApp.config', 'ngTouch', 'ui.router', 'ui.bootstrap',
      'ui.bootstrap.tpls', 'ngSanitize', 'ngResource', 'lbServices',
      'dialogs.main', 'dialogs.default-translations', 'ImageCropper',
      'dcbImgFallback', 'angularMoment','ui.bootstrap','ui.bootstrap.datetimepicker',
      'toggle-switch','ngCsv','summernote','ngFileUpload']);

  /********************************************
   END: BREAKING CHANGE in AngularJS v1.3.x:
   *********************************************/

  /* Setup global settings */
  VettiverFaxApp.factory('settings', ['$rootScope', function ($rootScope) {
    // supported languages settings
    var settings = {
      layout: {
        pageSidebarClosed: false, // sidebar menu state
        pageBodySolid: false, // solid body color state
        pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
      },
      layoutImgPath: Metronic.getAssetsPath() + 'admin/layout/img/',
      layoutCssPath: Metronic.getAssetsPath() + 'admin/layout/css/',
      imageCropSettings: {
        campaign: {
          minSize: [300,200],
          maxSize: [840,560],
          aspectRatio: 1
        }
      }
    }; // Image settings

    $rootScope.settings = settings;

    return settings;
  }]);


  /***
   Layout Partials.
   By default the partials are loaded through AngularJS ng-include directive. In case they loaded in server side(e.g: PHP include function) then below partial
   initialization can be disabled and Layout.init() should be called on page load complete as explained above.
   ***/

  VettiverFaxApp.config(function ($stateProvider, $urlRouterProvider,
                                dialogsProvider, LoopBackResourceProvider,
                                EnvironmentConfig) {
    dialogsProvider.useBackdrop('static');
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/views/dashboard.html',
        controller: 'DashboardCtrl'
      })
      .state('campaigns', {
        url: '/campaigns',
        abstract: true,
        template: '<div ui-view="" class="fade-in-up"></div>'
      })
      .state('campaigns.list', {
        url: '/list?title&picture&signatureCount&target&faxNumbers&faxTo&status&page',
        templateUrl: 'app/views/campaigns.html',
        controller: 'CampaignsCtrl'
      })
      .state('campaigns.create', {
        url: '/create',
        templateUrl: 'app/views/campaign_details.html',
        controller: 'CampaignDetailsCtrl',
        resolve: {
          campaign: ['Campaign', '$log',
            function resolveCampaign(Campaign, $log) {
              //$scope.editMode = true;
              return new Campaign({
                target: 1000,
                status: 'pending'
              });
            }]
        }
      })
      .state('campaigns.details', {
        url: '/:id',
        templateUrl: 'app/views/campaign_details.html',
        controller: 'CampaignDetailsCtrl',
        resolve: {
          campaign: ['Campaign', '$stateParams', '$log',
            function resolveCampaign(Campaign, $stateParams, $log) {
              return Campaign.findById({id: $stateParams.id},
                {/*include: 'initiator'*/},
                function onSuccess(campaign) {
                  $log.debug('resolveCampaign success');
                  campaign.faxNumbers = campaign.faxNumbers.join(',');
                },
                function onError() {
                  $log.error('resolveCampaign error');
                }
              ).
                $promise;
            }]
        }
      })
      .state('campaigns.details.edit', {
        url: '/edit',
        // template: '<div></div>',
        controller: function ($scope) {
          $scope.$parent.editMode = true;
        }
      })
      .state('users', {
        url: '/users',
        abstract: true,
        template: '<div ui-view="" class="fade-in-up"></div>'
      })
      .state('users.list', {
        url: '/list?or&created&role&page&selection&currentDevice&stepsToday&totalSteps&averageSteps&order&lastAppOpened',
        templateUrl: 'app/views/users.html',
        controller: 'UsersCtrl'
      })
      .state('users.create', {
        url: '/create',
        templateUrl: 'app/views/user_details.html',
        controller: 'UserDetailsCtrl',
        resolve: {
          user: ['User', function resolveUser(User) {
            return new User({});
          }]
        }
      })
      .state('users.details', {
        url: '/:id',
        templateUrl: 'app/views/user_details.html',
        controller: 'UserDetailsCtrl',
        resolve: {
          user: ['User', '$stateParams', '$log',
            function resolveUser(User, $stateParams, $log) {
              return User.findById({
                  id: $stateParams.id,
                  filter: {includeRoles: true}
                },
                function onSuccess() {
                  $log.debug('resolveUser success');
                },
                function onError() {
                  $log.error('resolveUser error');
                }
              ).$promise;
            }]
        }
      });
      /*.state('users.details.edit', {
       url: '/edit',
       // template: '<div></div>',
       controller: function ($scope) {
       $scope.$parent.editMode = true;
       }
       })*/

    $urlRouterProvider.otherwise('/');
    LoopBackResourceProvider.setUrlBase(EnvironmentConfig.API_URL);

  });


  /* Init global settings and run the app */
  VettiverFaxApp.run(["$rootScope", "settings", "$state", 'LoopBackAuth', 'User',
    '$stateParams', '$location', '$urlRouter', '$log',
    function ($rootScope, settings, $state, LoopBackAuth, User, $stateParams,
              $location, $urlRouter, $log) {
      if ($location.absUrl().indexOf('/login.html') > -1) {
        $log.debug('login');
        return;
      }
      $rootScope.$state = $state; // state to be accessed from view
      $rootScope.$stateParams = $stateParams;
      // set sidebar closed and body solid layout mode
      $rootScope.settings.layout.pageBodySolid = true;
      $rootScope.settings.layout.pageSidebarClosed = false;
      if ($location.absUrl().indexOf('/public.html') > -1) {
        $log.debug('public');
        return;
      }
      if (LoopBackAuth.accessTokenId) {
        $rootScope.$on('$stateChangeStart', function (event) {
          if ($rootScope.currentUser && $rootScope.currentUser.$resolved) {
            return;
          }
          // Halt state change from even starting
          event.preventDefault();
          $rootScope.currentUser = User.getCurrent(function onSuccess() {
            $urlRouter.sync();
            $rootScope.$broadcast('adminUserLoaded', $rootScope.currentUser);
          }, function onError() {
            window.location.href =
              '/login.html#/?' + 'next=' + $location.absUrl();
          });
        });
      }
      else {
        window.location.href = '/login.html#/?' + 'next=' + $location.absUrl();
      }

      /**
       *
       * @param user
       * @returns {*}
       */
      $rootScope.getDisplayName = function (user) {
        if (!user) {
          return 'undefined';
        }
        if (user.fullName) {
          return user.fullName;
        }
        var dispName = '';
        if (user.firstName) {
          dispName += user.firstName;
        }
        if (user.lastName) {
          if (user.firstName) {
            dispName += ' ';
          }
          dispName += user.lastName;
        }
        if (dispName !== '') {
          return dispName;
        }
        return user.username || user.email;
      };
      /**
       *
       * @param picture
       * @param size
       * @returns {*}
       */
      $rootScope.getImageWithSize = function (picture, size, isUser) {
        if (typeof size !== 'string') {
          size = size.toString();
        }
        if (!picture) {
          if (isUser) {
            switch (size) {
              case '32':
                return '/assets/avatars/light_on_gray/avatar_male_light_on_gray_32x32.png';
              default:
                return undefined;
            }
          }
          return undefined;
        }
        if (picture.renditions && picture.renditions[size]) {
          return picture.renditions[size];
        }
        if (!picture.dynamicUrl) {
          return picture.url;
        }
        var value = picture.dynamicUrl.replace(/\{\{size}}/g,
          size).replace(/\{\{accessToken}}/, LoopBackAuth.accessTokenId);
        return value;
      };

      $rootScope.logout = function (event) {
        User.logout(function onSuccess() {
          window.location.href =
            '/login.html#/?' + 'next=' + $location.absUrl();
        }, function (err) {
          $log.error('User logout error');
        });
      }
    }]);


})(window.Metronic);
